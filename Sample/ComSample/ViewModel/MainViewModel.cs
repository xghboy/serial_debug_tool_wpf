using ComSample.Common;
using ComSample.Enums;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows;

namespace ComSample.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ComParameterConfig = new ComParameterConfig();
            CurrentParameter = new CurrentParameter();
        }

        private ComParameterConfig comParameter;

        /// <summary>
        /// 参数类
        /// </summary>
        public ComParameterConfig ComParameterConfig
        {
            get { return comParameter; }
            set { comParameter = value; RaisePropertyChanged(); }
        }

        private CurrentParameter currentParameter;

        /// <summary>
        /// 当前配置参数
        /// </summary>
        public CurrentParameter CurrentParameter
        {
            get { return currentParameter; }
            set { currentParameter = value; RaisePropertyChanged(); }
        }

        #region Command

        private RelayCommand _ToOpen;
        public RelayCommand ToOpen
        {
            get
            {
                if (_ToOpen == null)
                {
                    _ToOpen = new RelayCommand(Open);
                }
                return _ToOpen;
            }
            set
            {
                _ToOpen = value;
            }
        }
        /// <summary>
        /// 根据配置打开端口
        /// </summary>
        public void Open()
        {
            this.CurrentParameter.Open();
        }

        private RelayCommand _ToClick;
        public RelayCommand ToClick
        {
            get
            {
                if (_ToClick == null)
                {
                    _ToClick = new RelayCommand(Click);
                }
                return _ToClick;
            }
            set
            {
                _ToClick = value;
            }
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        public void Click()
        {
            this.CurrentParameter.Send();
        }


        private RelayCommand _ToClear;

        public RelayCommand ToClear
        {
            get
            {
                if (_ToClear == null)
                {
                    _ToClear = new RelayCommand(Clear);
                }
                return _ToClear;
            }
            set
            {
                _ToClear = value;
            }
        }

        /// <summary>
        /// 清空接收区
        /// </summary>
        public void Clear()
        {
            this.CurrentParameter.Clear();
        }

        private RelayCommand _ToClearText;

        public RelayCommand ToClearText
        {
            get
            {
                if (_ToClearText == null)
                {
                    _ToClearText = new RelayCommand(ClearText);
                }
                return _ToClearText;
            }
            set
            {
                _ToClearText = value;
            }
        }

        /// <summary>
        /// 清空界面值
        /// </summary>
        public void ClearText()
        {
            this.CurrentParameter.ClearText();
        }

        #endregion
    }

    /// <summary>
    /// 当前配置参数
    /// </summary>
    public class CurrentParameter : ViewModelBase
    {
        #region Private

        private int baudRdate = 9600;
        private int dataBit = 8;
        private Port port;
        private CheckMode checkMode;
        private StopBit stopBit = StopBit.One;
        private SerialPort serialPort;

        private string dataReceiveInfo;
        private string sendData;
        private bool isOpen;
        private bool receiveFormat16 = true;
        private bool sendFormat16 = true;

        private int sendCount;
        private int receiveCount;

        #endregion

        #region UI绑定参数

        /// <summary>
        /// 发送数量
        /// </summary>
        public int SendCount
        {
            get { return sendCount; }
            set { sendCount = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 接收数量
        /// </summary>
        public int ReceiveCount
        {
            get { return receiveCount; }
            set { receiveCount = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 接收区16进制
        /// </summary>
        public bool ReceiveFormat16
        {
            get { return receiveFormat16; }
            set { receiveFormat16 = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 接收区数据
        /// </summary>
        public string DataReceiveInfo
        {
            get { return dataReceiveInfo; }
            set { dataReceiveInfo = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        public string SendData
        {
            get
            {
                return sendData;
            }
            set { sendData = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 发送区16进制
        /// </summary>
        public bool SendFormat16
        {
            get { return sendFormat16; }
            set { sendFormat16 = value; RaisePropertyChanged(); }
        }

        #endregion

        #region 串口参数信息

        /// <summary>
        /// 开关
        /// </summary>
        public bool IsOpen
        {
            get { return isOpen; }
            set { isOpen = value; RaisePropertyChanged(); }
        }


        /// <summary>
        /// 数据位
        /// </summary>
        public int DataBit
        {
            get { return dataBit; }
            set { dataBit = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRdate
        {
            get { return baudRdate; }
            set { baudRdate = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 端口
        /// </summary>
        public Port Port
        {
            get { return port; }
            set { port = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 校验
        /// </summary>
        public CheckMode CheckMode
        {
            get { return checkMode; }
            set { checkMode = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 停止位
        /// </summary>
        public StopBit StopBit
        {
            get { return stopBit; }
            set { stopBit = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// COM
        /// </summary>
        public SerialPort SerialPort
        {
            get { return serialPort; }
            set { serialPort = value; RaisePropertyChanged(); }
        }


        #endregion

        #region 串口操作方法

        /// <summary>
        /// 开启串口
        /// </summary>
        /// <returns></returns>
        public bool Open()
        {
            if (serialPort != null && serialPort.IsOpen)
            {
                return Close();
            }
            try
            {
                serialPort = new SerialPort();
                serialPort.DataBits = this.DataBit;
                serialPort.StopBits = ComHelper.GetStopBits(this.StopBit.ToString());
                serialPort.Parity = ComHelper.GetParity(this.CheckMode.ToString());
                serialPort.PortName = this.Port.ToString();
                serialPort.RtsEnable = true;
                serialPort.DataReceived += SerialPort_DataReceived;
                serialPort.Open();

                if (serialPort.IsOpen)
                    return IsOpen = true;
                else
                    return IsOpen = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return IsOpen = false;
        }

        /// <summary>
        /// 关闭串口
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            try
            {
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                    return IsOpen = serialPort.IsOpen;
                }
                else
                {
                    return IsOpen = serialPort.IsOpen;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return IsOpen = false;
            }
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        public void Send()
        {
            if (serialPort != null && serialPort.IsOpen)
            {

                if (SendFormat16)
                {
                    byte[] bytes = CRC.StringToHexByte(SendData);
                    this.SerialPort.Write(bytes, 0, bytes.Length);
                    SendCount = bytes.Length; //不做增量
                }
                else
                {
                    this.SerialPort.Write(SendData);
                    SendCount = SendData.Length;
                }
                Messenger.Default.Send("", "PlaySendFlashing");
            }
        }

        /// <summary>
        /// 清空接收区
        /// </summary>
        public void Clear()
        {
            this.DataReceiveInfo = string.Empty;
        }

        /// <summary>
        /// 清空发送区和缓存区
        /// </summary>
        public void ClearText()
        {
            this.SendData = string.Empty;
            this.SendCount = 0;
            this.ReceiveCount = 0;
        }

        #endregion

        /// <summary>
        /// 返回事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Messenger.Default.Send("", "PlayReciveFlashing");

            byte[] readBuffer = new byte[SerialPort.ReadBufferSize];
            SerialPort.Read(readBuffer, 0, readBuffer.Length);

            ReceiveCount = SerialPort.ReceivedBytesThreshold; //不做增量

            if (ReceiveFormat16)
            {
                //不做增量
                DataReceiveInfo = CRC.ByteToString(readBuffer, true);
            }
            else
            {
                DataReceiveInfo = Encoding.UTF8.GetString(readBuffer);
            }

        }
    }

    /// <summary>
    /// 串口参数设置类
    /// </summary>
    public class ComParameterConfig : ViewModelBase
    {
        public ComParameterConfig()
        {
            Port = System.Enum.GetValues(typeof(Port));
            CheckMode = System.Enum.GetValues(typeof(CheckMode));
            StopBit = System.Enum.GetValues(typeof(StopBit));
            BaudRate = new List<int>() { 110, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, };
            DataBit = new List<int>() { 6, 7, 8 };
        }

        private Array port;
        private Array checkMode;
        private Array stopBit;
        private List<int> dataBit;
        private List<int> baudRate;

        /// <summary>
        /// 端口
        /// </summary>
        public Array Port
        {
            get { return port; }
            set { port = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 校验模式
        /// </summary>
        public Array CheckMode
        {
            get { return checkMode; }
            set { checkMode = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 停止位
        /// </summary>
        public Array StopBit
        {
            get { return stopBit; }
            set { stopBit = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 波特率
        /// </summary>
        public List<int> BaudRate
        {
            get { return baudRate; }
            set { baudRate = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 数据位
        /// </summary>
        public List<int> DataBit
        {
            get { return dataBit; }
            set { dataBit = value; RaisePropertyChanged(); }
        }

    }
}
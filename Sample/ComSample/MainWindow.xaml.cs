﻿using ComSample.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComSample
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        MainViewModel mainWindow;

        public MainWindow()
        {
            InitializeComponent();
            mainWindow = new MainViewModel();

            this.DataContext = mainWindow;

            InitRegister();
        }

        /// <summary>
        /// 注册类
        /// </summary>
        private void InitRegister()
        {
            Messenger.Default.Register<string>(this, "PlayReciveFlashing", PlayReciveFlashing);//
            Messenger.Default.Register<string>(this, "PlaySendFlashing", PlaySendFlashing);//
        }

        /// <summary>
        /// 打开串口
        /// </summary>
        /// <param name="empty"></param>
        private void Open(string empty)
        {
            mainWindow.CurrentParameter.Open();
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="empty"></param>
        private void Click(string empty)
        {
            mainWindow.CurrentParameter.Send();
        }

        /// <summary>
        /// 无边框拖动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Move_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 清空界发送and缓存区
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            mainWindow.CurrentParameter.ClearText();
        }

        #region Animation

        /// <param name="emp"></param>
        private void PlaySendFlashing(string emp)
        {
            var ani = new ColorAnimation();
            ani.Duration = new TimeSpan(1000);
            ani.From = (Color)ColorConverter.ConvertFromString("#4EEE94");
            ani.To = (Color)ColorConverter.ConvertFromString("#EE3B3B");
            BlkSend.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, ani);
        }

        private void PlayReciveFlashing(string emp)
        {

            this.Dispatcher.Invoke(() =>
            {
                var ani = new ColorAnimation();
                ani.Duration = new TimeSpan(1000);
                ani.From = (Color)ColorConverter.ConvertFromString("#4EEE94");
                ani.To = (Color)ColorConverter.ConvertFromString("#EE3B3B");
                BlkRecive.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, ani);
            });

        }

        #endregion
    }
}
